{{-- @extends('layouts.app') --}}
@extends('layouts.app')
{{-- @include('layouts.navbar') --}}

@section('content')

<div class="container mb-4">
</div>
<div class="container">

    <div class=" container p-0 shadow mt-1 col-lg-10" id="contenedorHead" style="background-color: rgb(233 233 233); font-weight: 500; color: rgb(179 40 45);">
        <div class="row">
            <div class="d-flex justify-content-center align-items-center m-2">
                <h3 class="text-center mr-3">USUARIOS</h3>
                <a href="#" class="btn" title="Agregar usuario" onclick="addUser()">
                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-plus-circle-fill" viewBox="0 0 16 16">
                        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3z"/>
                    </svg>
                </a>
            </div>
        </div>  
    </div>

    <div class=" container p-4 shadow mt-2 col-lg-10" id="contenedor" style="background-color: rgb(233 233 233); font-weight: 500; color: rgb(179 40 45);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    
                </div>
            </div>
        </div> 
        <table class="table border-dark TBL" id="TBL" aria-describedby="TBL">
            <thead class="thead2 align-middle table-dark">
                    <tr >
                        <th scope="col" class="text-center">NOMBRE</th>
                        <th scope="col" class="text-center">CORREO</th>
                        <th scope="col" style="width: 10px;">ACCIÓN</th>
                    </tr>
                </thead>
        </table>
</div> 
</div>




<form id="formUser">
            @csrf
            <div class="modal fade" id="ModalUser" tabindex="-1" aria-hidden="true" aria-labelledby="modalTitle">
                <div class="modal-dialog modal-dialog-centered modal-md">
                    <div class="modal-content">
                        <div class="modal-header justify-content-center">
                            <h5 class="modal-title" style="color: black">Agregar usuario</h5>
                        </div>
                        <div class="modal-body" style="font-weight: bold;">
                            <div class="container row">
                              <div class="col-md-12">
                                <label class="form-label">Nombre:</label>
                                <input type="text" class="form-control" id="name" name="name" required>
                              </div>
                                <div class="col-md-12">
                                    <label class="form-label"> Apellido Paterno:</label>
                                    <input type="text" class="form-control" id="lastNamePaternal" name="lastNamePaternal" required>
                                </div>
                                <div class="col-md-12">
                                    <label class="form-label"> Apellido Materno:</label>
                                    <input type="text" class="form-control" id="lastNameMaternal" name="lastNameMaternal" required>
                                </div>
                                <div class="col-md-12">
                                    <label class="form-label"> Correo:</label>
                                    <input type="email" class="form-control" id="email" name="email" required>
                                </div>
                                <div class="col-md-12">
                                    <label class="form-label" id = "labelPass">Contraseña:</label>
                                    <input type="password" class="form-control" id="password" name="password">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer pr-8 pl-8">
                            <button type="button" class="btn btn-danger"
                                data-bs-dismiss="modal">Cancelar</button>
                            <button class="btn btn-success" type="submit"
                                style="float: right; color: white" >Aceptar</button>
                        </div>
                    </div>
                </div>
              </div>
    </form>




    <form id="formUpdateUser">
        @csrf
        <div class="modal fade" id="ModalUpdateUser" tabindex="-1" aria-hidden="true" aria-labelledby="modalTitle">
            <div class="modal-dialog modal-dialog-centered modal-md">
                <div class="modal-content">
                    <div class="modal-header justify-content-center">
                        <h5 class="modal-title" style="color: black">Actualizar usuario</h5>
                    </div>
                    <div class="modal-body" style="font-weight: bold;">
                        <div class="container row">
                          <div class="col-md-12">
                            <label class="form-label">Nombre:</label>
                            <input type="text" class="form-control" id="name2" name="name" required>
                          </div>
                            <div class="col-md-12">
                                <label class="form-label"> Apellido Paterno:</label>
                                <input type="text" class="form-control" id="lastNamePaternal2" name="lastNamePaternal" required>
                            </div>
                            <div class="col-md-12">
                                <label class="form-label"> Apellido Materno:</label>
                                <input type="text" class="form-control" id="lastNameMaternal2" name="lastNameMaternal" required>
                            </div>
                            <div class="col-md-12">
                                <label class="form-label"> Correo:</label>
                                <input type="email" class="form-control" id="email2" name="email" required>
                            </div>
                            <div class="col-md-12">
                                <label class="form-label mt-2" id="labelChange">Cambiar contraseña:</label>
                                <input type="checkbox" class="form-check-input mt-3" id="changePassword" name="changePassword">
                            </div>
                            <div class="col-md-12">
                                <input type="password" class="form-control" id="password2" name="password" hidden>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer pr-8 pl-8">
                        <button type="button" class="btn btn-danger"
                            data-bs-dismiss="modal">Cancelar</button>
                        <button class="btn btn-success" type="submit"
                            style="float: right; color: white" >Aceptar</button>
                    </div>
                </div>
            </div>
          </div>
</form>

@endsection
@section('script')
    <script type="text/javascript" src="{{ asset('/js/user.js') }}"></script>
@endsection