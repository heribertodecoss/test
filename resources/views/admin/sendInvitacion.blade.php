@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">{{ __('Enviar Invitación Por Correo') }}</div>

                <div class="card-body">
                    <form class="row gg-5" method="POST" id="sendInvitacion"">
                        @csrf
                        <div class="col-md-8 mb-2">
                                <label for="email" class="col-form-label text-md-end">{{ __('Correo Electronico: ') }}</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="correo electrónico">
                        </div>
                        <div class="col-md-4 mb-2">
                            <label for="perfil" class="col-form-label">Perfil: </label>
                            <select class="form-select" id="" name="perfil" required>
                                <option selected disabled value="">--seleccione--</option>
                                <option value="admin">Admin</option>
                                <option value="suministro">Suministro</option>
                                <option value="almacen">Almacen</option>
                                <option value="control_abasto">Control Abasto</option>
                            </select>
                        </div>

                        <div class="col-md" style="margin-top: 20px;">
                            {{-- <div class="col-md-12 offset-md-2"> --}}
                                <button id="btnEnviarInvitacion" type="submit" class="btn btn-success" style="width: 100%">
                                    {{ __('Enviar Invitación') }}
                                </button>
                            {{-- </div> --}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/afiliacion/admin.js') }}"></script>
@endsection