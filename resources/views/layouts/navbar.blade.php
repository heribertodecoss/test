
<nav class="navbar navbar-white navbar_h" style="height: 68px; box-shadow: 0 2px 5px 0 rgb(0 0 0 / 56%);">
    <div class="container" id="cabecera">
        
        <a class="navbar-brand" href="{{ url('/login') }}" style="font-family: Montserrat; font-size: 15px; padding: 0; color:white; display: flex; align-items: flex-end;">
          <img src="{{ asset('/img/icono.png') }}" style="height: 55px; margin-right: 10px;" alt="logo" id="logo">
            <div>
              TEST<br>
              <span style="font-size: 10px;">Sistema de Prueba</span>
            </div>
        </a>
        @guest
        @else
        <div class="container" id="cabecera_menu">
          <div class="dropdown">
                              
            <button class="btn btn-secondary dropdown-toggle" id="bton_nabvar" type="button" data-bs-toggle="dropdown" aria-expanded="false">
              {{ Auth::user()->name }}
            </button>
            
            <ul class="dropdown-menu">
                <li>
                  <button class="dropdown-item" type="button" href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                  {{ __('Cerrar sesión') }}

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                  </form></button>
                </li>
            </ul>
          </div>
          <button class="navbar-toggler" id="bton_menu" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasDarkNavbar" aria-controls="offcanvasDarkNavbar">
              <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-list" viewBox="0 0 16 16" style="color: white">
                <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/>
              </svg>
          </button>
          <div class="offcanvas offcanvas-end text-bg-dark menu" tabindex="-1" id="offcanvasDarkNavbar" aria-labelledby="offcanvasDarkNavbarLabel">
            <div class="offcanvas-header">
              <h5 class="offcanvas-title" id="offcanvasDarkNavbarLabel">Menú</h5>
              <button type="button" class="btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <hr style="border: 1px solid white; width: 100%;">
            <div class="offcanvas-body">
              <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
                  <li class="nav-item">
                      <a class="nav-link" href="/userIndex" style="color: white"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-people-fill" viewBox="0 0 16 16">
                        <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6m-5.784 6A2.24 2.24 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.3 6.3 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1zM4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5"/>
                      </svg> Usuarios</a>
                  </li>
              </ul>
            </div>
          </div>     
          @endguest         
    </div>
</nav>