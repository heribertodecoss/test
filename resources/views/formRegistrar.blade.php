@extends('layouts.app')

@section('content')
<div class="container">
    <input type="hidden" name="teken2" id="token2" value="{{ $token }}">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">{{ __('Registrarse') }}</div>

                <div class="card-body">
                    <form class="row gg-5" method="POST" id="formRegistrarse"">
                        @csrf
                        <div class="col-md-4 mb-2">
                                <label for="name" class="col-sm-4 col-form-label">{{ __('Nombre: ') }}</label>
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Nombre">
                        </div>

                        <div class="col-md-4">
                            <label for="name" class="col-form-label">{{ __('Apellido Paterno: ') }}</label>
                            <input id="apellido_paterno" type="text" class="form-control" name="apellido_paterno" value="{{ old('apellido_paterno') }}" required autocomplete="apellido_paterno" autofocus placeholder="apellido paterno">
                        </div>

                        <div class="col-md-4">
                            <label for="name" class="col-form-label">{{ __('Apellido Materno: ') }}</label>
                            <input id="apellido_materno" type="text" class="form-control" name="apellido_materno" value="{{ old('apellido_materno') }}" required autocomplete="apellido_materno" autofocus placeholder="apellido materno">
                        </div>

                        <div class="col-md-8 mb-2">
                                <label for="email" class="col-form-label text-md-end">{{ __('Correo Electronico: ') }}</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="correo electrónico">
                        </div>

                        <div class="col-md-4 mb-2">
                            <label for="email" class="col-form-label text-md-end">{{ __('Matricula: ') }}</label>
                            <input id="matricula" type="text" class="form-control" name="matricula" value="{{ old('matricula') }}" required autocomplete="matricula" placeholder="matrícula">
                        </div>

                        <div class="col-md-4 mb-2">
                            <label for="catalogo" class="col-form-label text-md-end">{{ __('Catalogo: ') }}</label>
                            <input id="catalogo" type="text" class="form-control" name="catalogo" value="{{ old('catalogo') }}" required autocomplete="catalogo" placeholder="catalogo">
                        </div>
                        <div class="col-md-4 mb-2">
                            <label for="telefono" class="col-form-label text-md-end">{{ __('Telefono: ') }}</label>
                            <input id="telefono" type="number" class="form-control" name="telefono" value="{{ old('telefono') }}" required autocomplete="telefono" placeholder="teléfono">
                        </div>
                        <div class="col-md-4 mb-2">
                        </div>

                        <div class="col-md-4 mb-2">
                            <label for="password" class="col-form-label text-md-end">{{ __('Contraseña: ') }}</label>
                            <input id="password" type="password" class="form-control" name="password" required autocomplete="new-password" placeholder="contraseña">
                        </div>

                        <div class="col-md-4 mb-2">
                            <label for="password-confirm" class="col-form-label text-md-end">{{ __('Confirmar Contraseña: ') }}</label>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="confirmar contraseña">
                        </div>

                        <div class="col-md-4" style="margin-top: 37px;">
                            {{-- <div class="col-md-12 offset-md-2"> --}}
                                <button id="btnguardarUsuario" type="submit" class="btn btn-success" style="width: 100%">
                                    {{ __('Registrarme') }}
                                </button>
                            {{-- </div> --}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/afiliacion/admin.js') }}"></script>
@endsection