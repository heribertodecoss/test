@extends("layouts.plantilla")
@section('contenido')
<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
    <div class=" container p-4 shadow mt-3 col-lg-9" id="contenedor" style="background-color: rgb(180, 180, 171);">
        <form class="row gg-5" id="afiliacion">
            @csrf
            <div class="col-md-3">
                <label for="validationCustom01" class="form-label">Distrito Federal:</label>
                <input type="number" class="form-control"  name="distrito_federal" placeholder="Número distrito federal"  required>
            </div>
            <div class="col-md-3">
                <label for="validationCustom01" class="form-label ">Sección Electoral:</label>
                <input type="number" class="form-control"  name="seccion_electoral" placeholder="Número de sección electoral" required>
            </div>
            <div class="col-md-3">
                <label for="" class="form-label" style="color: rgb(180, 180, 171);">.</label>
                <select class="form-select" id="" name="tipo_solicitud" required>
                    <option selected disabled value="">--seleccione una opción--</option>
                    <option value="Afiliación">Afiliación</option>
                    <option value="Ratificación">Ratificación</option>
                </select>
            </div>
            <div class="col-md-3 mb-2">
                <label for="validationCustom01" class="form-label">Fecha:</label>
                <input type="date" class="form-control" id="" name="fecha" value="" required>
            </div>

            <div class="col-md-4">
                <label for="validationCustom01" class="form-label">Nombre (s):</label>
                <input type="text" class="form-control" id="" value="" name="nombre" placeholder="Nombre" required>
            </div>
            <div class="col-md-4">
                <label for="validationCustom01" class="form-label">Primer Apellido:</label>
                <input type="text" class="form-control" id="" value="" name="apellido_paterno" placeholder="Primer Apellido" required>
            </div>
            <div class="col-md-4 mb-2">
                <label for="validationCustom01" class="form-label">Segundo Apellido:</label>
                <input type="text" class="form-control" id="" value="" name="apellido_materno" placeholder="Segundo Apellido" required>
            </div>
            <div class="col-md-8">
                <label for="validationCustom01" class="form-label">Calle:</label>
                <input type="text" class="form-control" id="" value="" name="calle" placeholder="Calle" required>
            </div>
            <div class="col-md-2">
                <label for="validationCustom01" class="form-label">Num. Ext:</label>
                <input type="number" class="form-control" id="" value="" name="numero_exterior" placeholder="Número Exterior" required>
            </div>
            <div class="col-md-2 mb-2">
                <label for="validationCustom01" class="form-label">Num. Int:</label>
                <input type="number" class="form-control" placeholder="Número Interior" name="numero_interior" id="" value="" required>
            </div>

            
            <div class="col-md-2">
                <label for="validationCustom01" class="form-label">Entidad:</label>
                <input type="text" class="form-control" id=""  name="entidad" value="CHIAPAS" readonly>
            </div>
            <div class="col-md-3">
                <label for="validationCustom01" class="form-label">Municipio / Alcadía:</label>
                <select class="form-select" id="municipio" name="municipio" required>
                    <option value="" disabled selected>--Seleccione Municipio--</option>
                    @foreach ($municipios as $municipio)
                        <option value="{{ $municipio->nom_mun }}">{{ $municipio->nom_mun }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <label for="validationCustom01" class="form-label">Localidad:</label>
                <select class="form-select" id="localidad" name="localidad" disabled required>
                    <option value="" disabled selected>--localidad--</option>
                </select>
            </div>
            <div class="col-md-2">
                <label for="validationCustom01" class="form-label">Colonia:</label>
                <input type="text" class="form-control" id="" value="" placeholder="Colonia" name="colonia" required>
            </div>
            <div class="col-md-2 mb-3">
                <label for="validationCustom01" class="form-label">C. P.:</label>
                <input type="number" class="form-control" id="" value="" placeholder="codigo postal" name="codigo_postal" required>
            </div>
            <div class="col-md-8">
                <label for="validationCustom01" class="form-label">Correo Electrónico (opcional):</label>
                <input type="email" class="form-control" id="" value="" placeholder="Correo electrónico" name="correo_electronico">
            </div>
            <div class="col-md-4 mb-2">
                <div class="row g-2">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Teléfono: </label>
                        <div class="col-sm-10">
                        <input type="number" class="form-control" id="inputEmail3" placeholder="Número de telefono" name="telefono">
                        </div>
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Celular: </label>
                        <div class="col-sm-10">
                        <input type="number" class="form-control" id="inputEmail3" placeholder="Número de celular" name="celular">
                        </div>
                </div>
            </div>
            <div class="col-md-6">
                <label for="validationCustom01" class="form-label">Clave de elector:</label>
                <input type="number" class="form-control" id="" value="" placeholder="Clave de elector" name="clave_elector" required>
            </div>
            <div class="col-md-6">
                <label for="validationCustom01" class="form-label">CURP,solo en caso de ser mayor a 15 años de edad y menor de 18 años:</label>
                <input type="number" class="form-control" id="" placeholder="Clave de CURP" value="" name="clave_curp" required>
            </div>
            <div class="col-12" style="text-align: end">
            <button class="btn btn-primary mt-2" id="btnguardar" type="submit" style="align-content: flex-end; background-color: rgb(179, 40, 45); border:none; outline:none;">
                <div style="display: flex">
                    <div class="spinner-border spinner-border-sm " id="loader" role="status"></div>
                    <h6 style="margin-left: 5px">Guardar</h6>
                </div>
            </button>
            </div>
        </form>
    </div>
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('/js/afiliacion/afiliacion.js') }}"></script>
@endsection
