<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->middleware('guest');
Auth::routes();


                                            //TEST
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/userIndex', [App\Http\Controllers\HomeController::class, 'userIndex']);//->middleware('auth');
Route::post('/getUsers', [App\Http\Controllers\HomeController::class, 'getUsers']);
Route::post('/saveUser', [App\Http\Controllers\HomeController::class, 'saveUser']);
Route::post('/updateUser', [App\Http\Controllers\HomeController::class, 'updateUser']);
Route::post('/deleteUser', [App\Http\Controllers\HomeController::class, 'deleteUser']);



//crear 700 usuarios
// Route::get('/createUsers', [App\Http\Controllers\HomeController::class, 'createUsers']);






  
 