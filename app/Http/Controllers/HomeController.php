<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // return view('home');
        return view('users.usersView');
    }

    public function userIndex()
    {
        return view('users.usersView');
    }

    public function getUsers(Request $request)
    {
        try{
            $users = \App\Models\User::all();
            return response()->json($users);
        }catch(\Exception $e){
            return response()->json($e->getMessage());
        }
    }

    public function saveUser(Request $request)
    {
        try{
            $request->validate([
                'name' => 'required',
                'lastNamePaternal' => 'required',
                'lastNameMaternal' => 'required',
                'email' => 'required|email',
                'password' => 'required',
            ]);

            $user = \App\Models\User::create([
                'name' => $request->name,
                'lastNamePaternal' => $request->lastNamePaternal,
                'lastNameMaternal' => $request->lastNameMaternal,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ]);
            if(!$user){
                return response()->json(['status' => false, 'message' => 'Error al crear el usuario']);
            }
            return response()->json(['status' => true, 'message' => 'Usuario creado correctamente']);

        }catch(\Exception $e){
            return response()->json($e->getMessage());
        }
    }

    public function updateUser(Request $request)
    {
        try{
            if($request->changePassword == 'on')
            {
                $request->validate([
                    'name' => 'required',
                    'lastNamePaternal' => 'required',
                    'lastNameMaternal' => 'required',
                    'email' => 'required|email',
                    'password' => 'required|min:8',
                ]);
            }else{
                $request->validate([
                    'name' => 'required',
                    'lastNamePaternal' => 'required',
                    'lastNameMaternal' => 'required',
                    'email' => 'required|email',
                ]);
            }
            $user = \App\Models\User::find($request->idUser);
            if(!$user){
                return response()->json(['status' => false, 'message' => 'Usuario no encontrado']);
            }
            $user->update([
                'name' => $request->name,
                'lastNamePaternal' => $request->lastNamePaternal,
                'lastNameMaternal' => $request->lastNameMaternal,
                'email' => $request->email,
                'password' => $request->changePassword == 'on' ? bcrypt($request->password) : $user->password,
            ]);
            return response()->json(['status' => true, 'message' => 'Usuario actualizado correctamente']);

        }catch(\Exception $e){
            return response()->json($e->getMessage());
        }
    }

    public function deleteUser(Request $request)
    {
        try{
            $user = \App\Models\User::find($request->idUser);
            if(!$user){
                return response()->json(['status' => false, 'message' => 'Usuario no encontrado']);
            }
            $user->delete();
            return response()->json(['status' => true, 'message' => 'Usuario eliminado correctamente']);
        }catch(\Exception $e){
            return response()->json($e->getMessage());
        }
    }




    public function createUsers()
    {
        try{
            for($i=0; $i<=700; $i++){
                \App\Models\User::create([
                    'name' => 'name'.$i,
                    'lastNamePaternal' => 'lastNamePaternal'.$i,
                    'lastNameMaternal' => 'lastNameMaternal'.$i,
                    'email' => 'correo'.$i.'@gmail.com',
                    'password' => bcrypt('password'.$i),
                ]);
            }
            return response()->json(['status' => true, 'message' => 'Usuarios creados correctamente']);
        }catch(\Exception $e){
            return response()->json($e->getMessage());
        }
    }

}
