
$(document).ready(function () {
    getAllUsers();
});

function getAllUsers(){
    $(".TBL").DataTable({
        "async": false,
        "destroy": true,
        "searching": true, 
        "paging": true,
        // "dom": 'Bfrtip',
        "ajax": {
            "url": "/getUsers",
            "type": "POST",
            "data": { "_token": $('#token').val() },
            "dataSrc": ""
        },
        "columns": [
            { 'data': 'name', 'class': 'text-center' },
            { 'data': 'email', 'class': 'text-center'},
            { 'data': 'id', 'class': 'text-center', "render": function (data, type, row, meta) {
                    return `<div style="display: flex; justify-content: center; padding: 4px;">
                            <a onclick="editarUsuario('${btoa(JSON.stringify(row))}')" class="btn btn-primary btn-sm" title="Editar Pregunta"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.5.5 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11z"/>
                                </svg></a>
                            <a onclick="deleteUser(${data})" style="margin-left: 8px;" class="btn btn-danger btn-sm" title="Eliminar Pregunta"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3-fill" viewBox="0 0 16 16">
                                <path d="M11 1.5v1h3.5a.5.5 0 0 1 0 1h-.538l-.853 10.66A2 2 0 0 1 11.115 16h-6.23a2 2 0 0 1-1.994-1.84L2.038 3.5H1.5a.5.5 0 0 1 0-1H5v-1A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5m-5 0v1h4v-1a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5M4.5 5.029l.5 8.5a.5.5 0 1 0 .998-.06l-.5-8.5a.5.5 0 1 0-.998.06m6.53-.528a.5.5 0 0 0-.528.47l-.5 8.5a.5.5 0 0 0 .998.058l.5-8.5a.5.5 0 0 0-.47-.528M8 4.5a.5.5 0 0 0-.5.5v8.5a.5.5 0 0 0 1 0V5a.5.5 0 0 0-.5-.5"/>
                                </svg></a>
                            </div>`;
            }},
        ],
        //oreder by
        "order": [[2, "desc"]],
        "autoWidth": false,
        "language": { url: '//cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json' }
    });
}

function addUser(){
    $('#formUser')[0].reset();
    $('#ModalUser').modal('show');   
}

$('#formUser').submit(function(ev) {
    ev.preventDefault();
    $.ajax({
        url: '/saveUser',
        type: 'POST',
        data: $('#formUser').serialize(),
        beforeSend: function () {
            $('#back').show()
        },
        success: function(data) {
            if (data.status) {
                Swal.fire(
                '¡Operación Exitosa!',
                data.message,
                'success'
                ).then(function() {
                    $("#formUser")[0].reset();
                    $('#ModalUser').modal('hide');
                    getAllUsers();
                })

            }else{
                Swal.fire(
                '¡Operación Errónea!',
                'Error al intentar guardar los datos: '+data,
                'error',
                )
            }
        },
        complete: function () {
            $('#back').hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Swal.fire(
                '¡Operación Errónea!',
                'Error: ' + xhr.status + ' ' + thrownError,
                'error'
                )
        }

    });

});

//onchange
$('#changePassword').change(function(){
    if(this.checked){
        $('#password2').removeAttr('hidden');
    }else{
        $('#password2').attr('hidden', 'hidden');
    }
});


let id_user;
function editarUsuario(row){
    row = JSON.parse(atob(row));
    id_user = row.id;
    $('#ModalUpdateUser').modal('show');
    $('#name2').val(row.name);
    $('#lastNamePaternal2').val(row.lastNamePaternal);
    $('#lastNameMaternal2').val(row.lastNameMaternal);
    $('#email2').val(row.email);
    $('#password2').val('');
}


$('#formUpdateUser').submit(function(ev) {
    ev.preventDefault();
    $.ajax({
        url: '/updateUser',
        type: 'POST',
        data: $('#formUpdateUser').serialize() + "&idUser=" + id_user,
        beforeSend: function () {
            $('#back').show()
        },
        success: function(data) {
            if (data.status) {
                Swal.fire(
                '¡Operación Exitosa!',
                data.message,
                'success'
                ).then(function() {
                    $("#formUpdateUser")[0].reset();
                    $('#ModalUpdateUser').modal('hide');
                    getAllUsers();
                })

            }else{
                Swal.fire(
                '¡Operación Errónea!',
                'Error al intentar guardar los datos: '+data,
                'error',
                )
            }
        },
        complete: function () {
            $('#back').hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Swal.fire(
                '¡Operación Errónea!',
                'Error: ' + xhr.status + ' ' + thrownError,
                'error'
                )
        }

    });

});










function deleteUser(idUser){
    Swal.fire({
        title: '¿Estás seguro de eliminar el registro?',
        text: "¡Esta acción no se puede revertir!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#dc3545',
        confirmButtonText: 'Si, eliminar'
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '/deleteUser',
                type: 'POST',
                data: { "_token": $('#token').val(), "idUser": idUser },
                beforeSend: function () {
                    $('#back').show()
                },
                success: function(data) {
                    if (data.status == true) {
                        Swal.fire(
                        '¡Operación Exitosa!',
                        data.message,
                        'success'
                        ).then(function() {
                            getAllUsers();
                        })
                    }else{
                        Swal.fire(
                        '¡Operación Errónea!',
                        'Error al intentar eliminar los datos: '+data,
                        'error',
                        )
                    }
                },
                complete: function () {
                    $('#back').hide();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    Swal.fire(
                        '¡Operación Errónea!',
                        'Error: ' + xhr.status + ' ' + thrownError,
                        'error'
                        )
                }
            });
        }
    })
}